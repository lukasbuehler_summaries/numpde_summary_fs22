% Week 7 continuation

\section{Beyond FEM: Alternative Discretizations}

    In this chapter we look at alternative discretizations and methods other than
    the previously seen Finite Element Methods (Chapter 3).

    \subsection{Finite Difference Methods (FDM)}

        \begin{quote}
            ``In numerical analysis, finite-difference methods (FDM) are a class 
            of numerical techniques for solving differential equations by 
            approximating derivatives with finite differences. Both the spatial 
            domain and time interval (if applicable) are discretized [...]''
            - Wikipedia
        \end{quote}

        \begin{recipe}[Construction of finite-difference methods]
            To construct the finite-difference method from a BVP in strong form
            you need to:
            \begin{enumerate}
                \item Use a structured mesh/grid.
                \item Replace derivatives with finite difference quotients, which
                    are anchored at the nodes / grid-points.
                \item Access solution values only at nodes / grid-points
            \end{enumerate}
        \end{recipe}

        \subsubsection{FDM for Two-Point BVPs}

            We start by looking at one-dimensional second-order two-point boundary value problems
            \[
                -\odv{}{x}\left(\sigma(x) \odv{u}{x}(x)\right) = f(x) \quad \text{in} \quad
                ]a,b[, \quad u(a) = u_a, \quad u(b) = u_b
            \]

            Let's follow the recipe from above.

            \begin{enumerate}
                \item Start with the mesh/grid.

                    \[
                        \mathcal{V}(\mathcal{M}) := \{ a = x_0 < x_1 < \cdots < x_{M-1} < x_M = b \}
                    \]
                    with $h_j := x_j - x_{j-1}$
                
                \item Replace derivatives with difference quotients.
                
                    \[
                        \odv{u}{x}(x_j) \approx \frac{u(x_j + h_{j+1}) - u(x_j - h_j)}{h_j + h_{j+1}}
                        = \frac{u(x_{j+1}) - u(x_{j-1})}{x_{j+1} - x_{j-1}}
                    \]
                
                    \begin{center}
                        \begin{tikzpicture}
                            %\draw [help lines, dashed,ystep=10] (-0.25,-0.1) grid(5,1.1);
                            \draw[thick, ->] (-2.5,0) -- (3.5,0);
                            \draw[thick, -] (-2,-0.1) -- (-2,3);
                            \draw[thick, -] (0,-0.1) -- (0,2);
                            \draw[thick, -] (3,-0.1) -- (3,2.5);

                            \draw[red, thick, -] (-2,3) -- (0,2);
                            \draw[red, thick, -] (0,2) -- (3,2.5);
                            \draw[red, dashed] (-2,2) -- (0,2);
                            \draw[red, dashed] (0,2) -- (3,2);


                            \node at (-2,-0.4) {$x_{j-1}$};
                            \node at (0,-0.4) {$x_j$};
                            \node at (3,-0.4) {$x_{j+1}$};

                            \node at (-1,0.3) {$h_j$};
                            \node at (1.5,0.3) {$h_{j+1}$};

                            \node[blue] at (-2,3.4) {$u(x_{j-1})$};
                            \node[blue] at (-2,3) {\textbullet};

                            \node[blue] at (0,2.4) {$u(x_j)$};
                            \node[blue] at (0,2) {\textbullet};

                            \node[blue] at (3,2.9) {$u(x_{j+1})$};
                            \node[blue] at (3,2.5) {\textbullet};
                        \end{tikzpicture}
                    \end{center}

                    However, if we were take the second derivatives, than we would need 
                    grid values from $x_{j-2}$ to $x_{j+2}$, which is undesirable.\\

                    We use the following trick: Introducing halfway points between 
                    the nodes. However, we can only evaluate $u$ on the nodes,
                    but when using the difference quotients twice, like for this
                    second-order problem we are looking at, we arrive at the
                    neighbouring nodes, so all is well.

                    Let me illustrate: To solve a second-order problem like this one, where evaluations
                    of $u$ have to lie on grid nodes but evaluations of $\sigma$ don't,
                    we use the trick to introduce points at the halfway points between the nodes, like so:
                    $x_{j+\onehalf}$ and $x_{j-\onehalf}$.

                    \[
                        x_{j+\onehalf} := \onehalf(x_j + x_{j+1}), \qquad
                        x_{j-\onehalf} := \onehalf(x_{j-1} + x_{j})
                    \]

                    And their derivates, evaluate $u$ on the nodes again.

                    \[
                        \odv{u}{x}\left(x_{j+\onehalf}\right) \approx \frac{u(x_{j+1}) - u(x_j)}{h_{j+1}}, \qquad
                        \odv{u}{x}\left(x_{j-\onehalf}\right) \approx \frac{u(x_{j}) - u(x_{j-1})}{h_j}
                    \]

                    But we make the first derivate to use these halfway points.
                    Looking at the problem from above:

                    \begin{align*}
                        -\odv{}{x}\left(\sigma(x) \odv{u}{x}(x)\right)_{|x = x_j} 
                        &\approx -\frac{1}{\frac{h_j}{2}+\frac{h_{j+1}}{2}}\left(
                        \sigma(x_{j+\onehalf}) \odv{u}{x}(x_{j+\onehalf}) 
                        -\sigma(x_{j-\onehalf}) \odv{u}{x}(x_{j-\onehalf})\right)\\
                        &= \frac{2}{h_j+h_{j+1}}\left(
                            \sigma(x_{j-\onehalf}) \odv{u}{x}(x_{j-\onehalf})
                            -\sigma(x_{j+\onehalf}) \odv{u}{x}(x_{j+\onehalf})\right) \\
                        &\approx \frac{2}{h_j+h_{j+1}}\left(
                            \sigma(x_{j-\onehalf}) \frac{u(x_{j}) - u(x_{j-1})}{h_j}
                            -\sigma(x_{j+\onehalf}) \frac{u(x_{j+1}) - u(x_j)}{h_{j+1}}\right)
                    \end{align*}

                    Leading us to the following solution for every interior grid point 
                    for the given problem.
                    \[
                        \Rightarrow \quad \frac{2}{h_j+h_{j+1}}\left(
                            \sigma(x_{j-\onehalf}) \frac{u(x_{j}) - u(x_{j-1})}{h_j}
                            -\sigma(x_{j+\onehalf}) \frac{u(x_{j+1}) - u(x_j)}{h_{j+1}}\right) = f(x_j)
                    \]

                \item Now we can access the solution values only at the grid nodes.
            
                    We want to build an LSE ($\matr{A}\vect{\mu} = \vect{\varphi}$) 
                    to solve this problem numerically.\\

                    We have $M+1$ grid points, and want to solve for this solution vector:
                    \[
                        \mu_j \approx u(x_j), \quad j=0, ..., M+1
                    \]

                    For every interior gridpoint we have:
                    \[
                        -\frac{\sigma(x+\onehalf)}{\onehalf h_{j+1}(h_j + h_{j+1})}\mu_{j+1} 
                        +\frac{h^{-1}_{j+1}\sigma(x_{j+\onehalf}) + h^{-1}_j \sigma(x_{j-\onehalf})}{\onehalf(h_j + j_{j+1})}\mu_j
                        -\frac{\sigma(x-\onehalf)}{\onehalf h_{j}(h_j + h_{j+1})}\mu_{j-1} 
                        = f(x_j)
                    \]

                    This gives us a LSE with $M-1$ equations and $M+1$ unknowns,
                    meaning we need to use boundary conditions to solve the problem.
                
            \end{enumerate}
                    
            \begin{example}[FDM for Dirchliet 2-point BVP]
                // TODO
            \end{example}

            \begin{example}[FDM for Neumann 2-point BVP]
                // TODO
            \end{example}

        \subsubsection{FDM in Two Dimensions}

            \begin{example}[Model problem and structured mesh]
                We are looking at this problem:
                \[
                    -\Delta u = -\pdv[order={2}]{u}{x_1} -\pdv[order={2}]{u}{x_2} = f, \quad
                    \text{in} \ \Omega := ]0,1[^2, \quad u=0 \ \text{on} \ \partial \Omega
                \]
                
                This gives us a $N^2 \times N^2$ linear system of equations.
            \end{example}

            \subsubsubsection{Stencil notation}

                In the above example, and in 2D FDM in general our unknowns are 
                located at grid points. We can visualize that in a grid as well.

                // TODO insert stencil notation graphic

                The LSE is described by a single stencil.

                \begin{example}[Stencils on more general meshes]
                    // TODO insert equilaterand triangular mesh stencil
                \end{example}

        \subsubsection{Finite Differences and Finite Elements}

            For 2-pt boundary value problems FEM and FDM produce the same LSE!

            \subsubsubsection{FDM vs. FEM in 2D}

                In 2D the LSE from FDM and p.w. Lagrangian FEM is also the same.

            \subsubsection{FDM Takeaways}

                Most finite difference schemes produce the same discretized problem
                as finite element Galerkin schemes with numerical quadrature on
                structured meshes.\\

                However, FDMs have difficulties handling complex geometries 
                (unstructured meshes).
                
                But efficient implementation is possible on structured meshes.

                \begin{recipe}[FDM Summary]
                    // TODO refine
                    \begin{enumerate}
                        \item  Mesh (best structured)
                        \item Approximate derivatives with difference quotients
                        \item Build LSE
                    \end{enumerate}
                \end{recipe}

        \pagebreak
    \subsection{Finite Volume Methods (FVM)}

        Finite Volume Methods are a class of numerical techniques for solving
        differential equations. 

        A benefit of using Finite Volume Methods is that they are easy
        to use on unstructured meshes.

        \subsubsection{Discrete Balance Laws}

            \begin{quote}
                The idea of FVM is to discretize the problem and enforce the 
                conservation/balance law for all the finitely many control volumes.
            \end{quote}

            \begin{definition}[Balance law]
                \[
                    \int_{\partial V} \vect{j} \cdot \vect{n} \ dS(\vect{x}) 
                    = \int_V f(\vect{x}) \ d\vect{x} \qquad
                    \text{for all ``control volumes''} \ V
                \]
                where $\vect{j}$ is the \emph{flux}. Example flux for Fourier's law:
                $\vect{j} = -\kappa(\vect{x}) \grad u$\\

                The \emph{balance law} says the flux over the surface of a control
                volume is equal to the change in the volume.
            \end{definition}

            \subsubsection{Control volumes}

                The first step is to choose polygonal cells (our control volumes) 
                on a mesh $\tilde{\mathcal{M}} = \{C_i\}_i$ covering the 
                computational domain $\Omega$. The control volumes are non-overlapping
                with $C_i, \ i=1, ..., \tilde{M}$. Where $\tilde{M} := \#\tilde{\mathcal{M}}$\\

                // TODO insert graphic of control volumes partition.\\

                

                We associate a nodal value $\mu_i$ with every cell $C_i$. We do
                this by creating nodes $\vect{p_i}$, which lie at the `center' of $C_i$.

                \[
                    \mu_i \approx u(\vect{p}_i)
                \]

                Side note: Instead of choosing a center like above, we could also
                set $\mu_i$ as:
                \[
                    \mu_i \approx \frac{1}{|C_i|} \int_{C_i} u(\vect{x}) d\vect{x}
                \]

            \subsubsubsection{From discrete flux to system of equations}

                The second step are numerical fluxes.\\

                \begin{definition}[Numerical flux]
                    For two adjacent cells $C_k$, $C_i$ with common edge 
                    $\Gamma_{ik} := \bar{C_i} \cap \bar{C_k}$ the \emph{numerical flux}
                    is defined as
                    \[
                        J_{ik} = \Psi(\mu_i, \mu_k) 
                        \approx \int_{\Gamma_{ik}} \vect{j} \cdot \vect{n}_{ik} \ dS 
                    \]
                    where $\Psi$ is the numerical flux function, $\vect{j}$ the flux
                    and $\vect{n}_{ik}$ is the edge normal.
                \end{definition}

                Now we need to obtain a system of equations combining the balance
                law and the numerical flux.

                \[
                    \int_{\partial C_i} \vect{j} \cdot \vect{n}_i \ dS = \int_{C_i} f(\vect{x}) \ d\vect{x}
                    \quad \Rightarrow \quad 
                    \sum_{k \, \in \, \mathcal{U}_i} J_{ik} = \int_{C_i} f(\vect{x}) \ d\vect{x}
                \]
                where $\mathcal{U}_i := \{j: C_i \ \text{and} \ C_j \ 
                \text{share edge}, \ C_j \in \tilde{\mathcal{M}} \}$

                $\mathcal{U}_i$ is the set of all cells that share an edge with $C_i$.\\

                This leads to a system of $\tilde{M}$ equations: (with $\tilde{M}$ unknowns)

                \[
                    \sum_{k \, \in \, \mathcal{U}_i} \Psi(\mu_i, \mu_k) 
                    = \int_{C_i} f(\vect{x}) \ d\vect{x} \quad \forall i=1, ..., \tilde{M}
                \]

                Using numerical 1-point quadrature we get
                \[
                    \sum_{k \, \in \, \mathcal{U}_i} \Psi(\mu_i, \mu_k) 
                    = |C_i| f(\vect{p}) \quad \forall i=1,...,\tilde{M}
                \]

        \subsubsection{Dual Meshes}

            What even are dual meshes?
            \begin{quote}
                Dual meshes are a technique to construct control volumes for FVM.
                They are based on conventional finite element triangulation.

                There are different types of dual meshes. We construct dual meshes by
                first constructing a primal mesh and then using a simple rule to obtain
                the dual mesh for a primal mesh.
            \end{quote}

            We will first learn about a specific type of dual meshes, the \emph{Voronoi dual meshes}
            and afterwards about the \emph{Barycentric dual meshes}.

            \subsubsubsection{Voronoi dual meshes}

                Voronoi meshes have the property that all points in a cell have
                a closer distance to the cell node than any other node.\\

                // TODO insert voronoi mesh graphic

                \begin{recipe}[Construction of Voronoi dual mesh]
                    To obtain a dual Voronoi mesh we
                    \begin{enumerate}
                        \item Construct a triangular mesh with nodes 
                            $\mathcal{V}(\mathcal{M}) =  \{ \vect{p}_1, ..., \vect{p}_M\}$.
                        \item Define the Voronoi cells
                        
                            \[
                                C_i := \{ \vect{x} \in \Omega: \ |\vect{x} - \vect{p}_i|
                                < |\vect{x} - \vect{p}_j| \ \forall j \neq i \}
                            \]
                    \end{enumerate}

                    Thus obtain the \emph{Voronoi dual mesh} $\tilde{\mathcal{M}}
                    := \{ C_i \}^M_{i=1}$
                \end{recipe}

                Properties of the Voronoi dual mesh:
                \begin{itemize}
                    \item The edges of the dual mesh are perpendicular bisectors of the 
                    edges of the primal mesh.
                    \item The nodes of the dual mesh are the
                    circumcenters of the triangles of the primal mesh.
                \end{itemize}
                 

            \subsubsubsection{Geometric construction}

                If we have an obtuse angle (more than 90 degrees) in a triangle
                when constructing the dual Voronoi mesh, the circumcenter lies 
                outside the triangle and the geometric construction breaks down.\\

                We can solve this by defining a rule that prevents this from 
                happening or use a triangulation that prevents this from happening.\\

                First, a possible rule is the following angle condition:

                \begin{thm}[Angle condition for Voronoi dual meshes]
                    Thm. 4.2.2.4

                    This \emph{Angle condition} ensures:
                    \[
                        \bar{C}_i \cap \bar{C}_j \neq \emptyset \quad \iff \quad 
                        \text{nodes} \ i, j \ \text{connected by an edge of} \ \mathcal{M}
                    \]
                    if
                    \begin{itemize}
                        \item sum of angles facing interior edge $\leq \pi$
                        \item angles facing boundary edges $\leq \frac{\pi}{2}$
                    \end{itemize}
                    both are satisfied.
                \end{thm}

                We can also solve the above mentioned problem choosing a specific 
                triangulation that avoids the problem like \emph{Delaunay triangulation}. 

                \begin{definition}[Delaunay triangulation]
                    Triangular meshes satisfying the angle conditions:
                    \begin{enumerate}[i)]
                        \item sum of angles facing interior edge $\leq \pi$
                        \item angles facing boundary edges $\leq \frac{\pi}{2}$
                    \end{enumerate}
                    are called \emph{Delaunay triangulations}
                \end{definition}

            \subsubsubsection{Barycentric dual meshes}

                A different type of dual meshes are the \emph{Barycentric dual meshes}.

                \begin{definition}[Barycenter of a triangle]
                    The \emph{barycentre} of a triangle with the three vertices
                    $\vect{a}^1, \vect{a}^2, \vect{a}^3$ is the point
                    \[
                        \vect{p} := \frac{1}{3}(\vect{a}^1 + \vect{a}^2 + \vect{a}^3)
                    \]
                \end{definition}

                \begin{recipe}[Construction of Barycentric dual mesh]
                    We again start from a triangular mesh as the primal mesh.\\

                    The dual edges are the union of the lines connecting the barycenters 
                    of the primal triangles and the midpoints of the primal edges.\\

                    For the dual nodes we choose the the barycenters of triangles
                    and the midpoints of the primal edges.
                \end{recipe}

                The construction of the Barycentric dual mesh is always possible.

            \subsubsubsection{FVM: Incorporation of Dirichlet boundary conditions}

                When applying Dirichlet boundary conditions to Barycentric meshes 
                we can use the following trick.\\

                Assume we have the Dirichlet boundary condition $u = g$ on $\partial \Omega$.\\

                We recall that the centers of the Barycentric dual cells lie on
                the triangular mesh vertices. And for all boundary cells they will
                lie on the boundary of the mesh. Meaning to incorporate the 
                boundary conditions we can set $\mu_i = g$.

        \subsubsection{Relationship of FVM and FEM}

            We can now see that finite volume methods and fintie element Galerkin 
            discretizations are closely related.

            \subsubsubsection{Numerical flux by interpolation}

                We are still missing a specification for the numerical flux function.

                We try to use \emph{Fourier's law}
                \[
                    \vect{j}(\vect{x}) = - \vect{\kappa}(\vect{x}) 
                    \grad \vect{u}(\vect{x}), \quad \vect{x} \in \Omega
                \]

                applied to a sufficiently smooth discretized $u_h$, reconstructed 
                from the dual cell values of $\mu_i$. The idea is to use a piecewise
                linear interpolant of $\mu_i$ to approximate $u_h$.

                \[
                    u \approx u_h = I_1 \vect{\mu} := \sum_{i=1}^N \mu_i b_h^i
                \]
                where $N$ is the number of dual cells.

                We choose the numerical flux 
                \[
                    I_{ik} = -\int_{\Gamma_{ik}} \grad I_1 \vect{\mu} \cdot \vect{n}_{ik} \ dS
                \]
                where $\Gamma_{ik}$ is the interface separating the control volumens $C_i$ and $C_k$.

                // TODO ...

                \begin{quote}
                    The finite volume discretization and the finite element 
                    Galerkin discretization spawn the same system matrix for 
                    this model problem.
                \end{quote}

            \begin{recipe}[FVM Summary]
                // TODO
            \end{recipe}

        \pagebreak
    \subsection{Spectral (Polynomial) Galerkin Methods}

        \begin{recipe}[Spectral Galerkin approach]
            ah
            \begin{itemize}
                \item Trial/test spaces of \emph{globally $C^\infty$-smooth} functions
                \item Globally supported basis functions
            \end{itemize}
        \end{recipe}

        The second feature leads to dense Galerkin matrices.

        \subsubsubsection{Polynomial Spectral Galerkin Methods}

            Idea:
            \begin{quote}
                Use globally $C^\infty$-smooth functions for trial/test-spaces
                and globally supported basis functions.
            \end{quote}

            \begin{itemize}
                \item for $d = 1$, $\Omega =\  ]a,b[$

                    This is easy, choose:
                    \[
                        V_{0,h} := \{ x \to (x-a)(x-b) q(x) \} \subset H^1_0(]a,b[)
                    \]

                \item for $d > 1$ this is only possible for \emph{tensor-product domains}.
                
                    For example for $d=2$, $\Omega = \ ]a_1, b_1[ \times ]a_2, b_2[$

                    we choose:
                    \[
                        V_{0,h} = \{ v(x_1, x_2) = (x_1, a_1)(b_1 - x_1)q_1(x_1)
                            \cdot (x_2 - a_2)(b_2 - x_2)q_2(x_2), \ q_1, q_2 \in \mathcal{P}_p(\R)
                        \}
                    \]
            \end{itemize}

        \subsubsubsection{Choice of basis for polynomial spectral Galerkin methods}

            Choose Intergrated Legendre Polynomials over Monomial type basis

            // TODO more on that...

            \begin{example}[Conditioning of Polynomial Spectral Galerkin Matrix]
                // TODO
            \end{example}

        \subsubsubsection{Convergence of Polynomial Spectral Galerkin Solutions}

            The error can converge exponentially, which is much better than the 
            otherwise seen algebraic convergence, if u has an analytic extention

            // TODO more on that

        \subsubsubsection{Numerical quadrature for polynomial spectral Galerkin methods}

            // TODO is there even enough in the slides here lol

        \subsubsubsection{Takeaways}

            \begin{itemize}
                \item Possibility to achieve \emph{exponential convergence}, if 
                    the exact solution possesses an analytic extension.

                \item But only mere algebraic convergence in case of limited
                    smoothness of the solution.

                \item Implementation is confned to only simple domains
                
                \item Densely populated Galerkin matrices
            \end{itemize}

        \pagebreak
    \subsection{Collocation Methods}

        \begin{quote}
            The idea is to choose a 
            finite-dimensional space of candidate solutions (usually polynomials 
            up to a certain degree) and a number of points in the domain 
            (called collocation points), and to select that solution which 
            satisfies the given equation at the collocation points. 

            \hfill- stolen from Wikipedia
        \end{quote}

        We focus on the classical view of PDEs/BVPs. For example:
        \[
            -\text{div}(\alpha(\vect{x}) \grad u) = f 
            \quad \text{in} \ \Omega, \quad u = g \ \text{on} \ \partial\Omega
        \]
        with $f,g \in C^0$, $\alpha \in C^1$, $u \in C^2(\Omega)$\\

        We now use the \emph{differential operator} $\mathsf{L}$ and the 
        \emph{boundary operator} $\mathsf{B}$, which are both linear.
        \[
            \mathsf{L}u = f \quad \text{in} \ C^0(\Omega), \qquad 
            \mathsf{B}u = g \quad \text{in} \ C^0(\partial\Omega)
        \]
        

        \begin{recipe}[Idea of collocation discretization]
            The general idea of collocation discretization is the following:
            \begin{enumerate}
                \item Seek an approximate solution $u_h$ in a finite-dimensional 
                    \emph{trial-space}

                    \[
                        V_{0,h} \subset C^2(\Omega), \quad N := \dim V_{0,h} < \infty
                    \]

                \item Pick a finite number of \emph{collocation nodes (points)}.
                
                    \[
                        \vect{x}_1, ..., \vect{x}_n \in \Omega, \quad 
                        \vect{y}_1, ..., \vect{y}_m \in \partial\Omega, \quad
                        m,n \in \mathbb{N}
                    \]

                \item Impose collocation conditions:
                
                    \[
                        u_h \in V_{0,h}: \quad \begin{array}{l}
                            (\mathsf{L} u_h)(\vect{x}_j) = f(\vect{x}_j), \quad j=1, ..., n,\\
                            (\mathsf{B} u_h)(\vect{y}_j) = g(\vect{y}_\ell), \quad j=1, ..., m
                        \end{array}
                    \]
            \end{enumerate}
        \end{recipe}

        \subsubsubsection{Choice of Collocation points}

            // TODO

        \subsubsubsection{From collocation conditions to system of equations}

            // TODO

        \subsubsection{Spectral Collocation Method}

        \subsubsection{Spline Collocation Method}

        \subsubsection{Assessment and Takeaway}

            \begin{itemize}
                \item Numerical quadrature is not required (good)
                \item Implementation is confined to simple domains (bad)
                \item Weaker theoretical guarantees (bad)
            \end{itemize}

