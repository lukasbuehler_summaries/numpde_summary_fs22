\section{Numerical Integration - Single-Step Methods}

    This chapter is about Numerical Integration. It is important to know that
    Numerical Integration is not Numerical Quadrature. Numerical Quadrature is 
    about the appoximate evaluation of integrals, whereas Numerical Integration
    is a method to solve ODEs.

    ODE stands for \emph{ordinary differential equation} and is a special case
    of a differential equation where the unknown function depends on a single
    independent variable (often representing time in our case).

    ODEs have to be supplied with \emph{initial values} in order to arrive at a
    meaningful (well-posed) \emph{initial-value problem} (IVP), for which we can 
    expect existance and uniqueness of solutions.

    \subsection{Initial-Value Problems for ODEs}

        In this section we introduce notations and fundamental concepts.

        \subsubsection{Ordinary Differential Equations (ODEs)}

            \begin{definition}[First-order Ordinary differential equation (ODE)]
                A \emph{first-order ordinary differential equation} (ODE) is an 
                equation of the form
                \[
                    \dot{\vect{y}} := \odv{\vect{y}}{t}(t) = \vect{f}(t, \vect{y}(t))
                \]
                with $\vect{f}: I \times D \to \R^N$ - A continuous right 
                hand side function of time $t \in I \subset \R$ and state 
                $\vect{y} \in D \subset \R^N, N \in \mathbb{N}$. With $I$ being 
                the time-interval and $D$ being the \emph{state space}.
                
                The ``dot''-notation from Newton indicates a derivative with 
                respecto to time $t$.\\

                For $N > 1 \quad \dot{\vect{y}} = \vect{f}(t, \vect{y})$ can be 
                viewed as a \emph{system of scalar ordinary differential equations}.
                \[
                    \dot{\vect{y}} = \vect{f}(t, \vect{y}) \iff
                    \begin{pmatrix}
                        \dot{y}_1\\ \vdots\\ \dot{y}_N
                    \end{pmatrix}
                    = \begin{pmatrix}
                        f_1(t, y_1, \cdots, y_N)\\
                        \vdots\\
                        f_N(t, y_1, \cdots, y_N)
                    \end{pmatrix}
                \]
            \end{definition}

            \begin{definition}[Scalar ODE]
                An ODE is called \emph{scalar} in the case $N = 1$:
                \[
                    \dot{y} = a y, \ a \in \R
                \]
            \end{definition}

            A solution of an ODE describes a continuous trajectory in state space.

            \begin{definition}[Solution of an ordinary differential equation]
                A \emph{solution of the ODE} $\dot{\vect{y}} = \vect{f}(t, \vect{y})$
                with continuous right hand side function $\vect{f}$ is a continuously
                differentiable function of time 
            \end{definition}

            \begin{lemma}[Space of solutions of linear ODEs]
                Lemma 6.1.1.11

                The set of solutions $\vect{y}: I \to \R^N$ is a \emph{vector space}.
            \end{lemma}

            The smoothness of the solutions of an ODE are inherited from its 
            right hand side funciton $\vect{f}$. 

            \begin{lemma}[Smoothness of solutions of ODEs]
                (Lemma 6.1.1.3)

                If the function $\vect{f}$ is $r$-times continuously differentiable with 
                respect to both arguments, then the trajectory is $r+1$ times 
                continuously differentiable in $I$.
            \end{lemma}


            \begin{definition}[Linear ODE]
                A first-order ODE is \emph{linear} if
                \[
                    \vect{f}(t, \vect{y}) = \matr{A}(t) \vect{y}, \quad
                    \matr{A}: I \to \R^{N,N} \quad \text{a contious function}
                \]
                Meaning the right hand side is a simple linear transformation on 
                $\vect{y}$ like so:
                \[
                    \vect{f}(t, \vect{y})
                    = \begin{bmatrix}
                        a_{11} & a_{12} & \cdots & a_{1N}\\
                        a_{21} & a_{22} & & a_{2N}\\
                        \vdots & & \ddots & \vdots\\
                        a_{N1} & a_{N2} & \cdots & a_{NN}
                    \end{bmatrix}
                    \begin{pmatrix}
                        y_1\\
                        \vdots\\
                        y_N
                    \end{pmatrix}
                    = \begin{pmatrix}
                        a_{11} y_1 + a_{12} y_2 + \cdots + a_{1N} y_N\\
                        a_{21} y_1 + a_{22} y_2 + \cdots + a_{2N} y_N\\
                        \vdots\\
                        a_{N1} y_1 + a_{N2} y_2 + \cdots + a_{NN} y_N
                    \end{pmatrix}
                \]
                where $a_{ii} := a_{ii}(t)$ are time dependent.\\

                If $\matr{A}$ does not depend on time, the ODE is known as a
                linear ODE with \emph{constant coefficients}.
            \end{definition}

            \begin{definition}[Autonomous ODE]
                Definition 6.1.2.4

                An ODE is \emph{autonomous} if it does not depend on time, but
                only state.
            \end{definition}

            \subsubsubsection{Solving a linear ODE with constant coefficients by diagonalization}
                If we have an ODE $\dot{\vect{y}} = \matr{A}\vect{y}$, with 
                $\matr{A} \in \R^{N,N}$ not depending on time, we can choose 
                $I = \R$ and the ODE can be solved by a \emph{diagonalization
                technique}.

                // TODO Diagonalization

        \subsubsection{Mathematical Modeling with ODEs: Examples}

            // TODO add examples for ODEs

        \subsubsection{Theory of Initial-Value-Problems (IVPs)}

            \begin{definition}[Generic Initial Value Problem (IVP)]
                (6.1.3.2)

                A \emph{generic Initial value problem} for a first-order ordinary
                differential equation (ODE) seeks a function $\vect{y}: I \to D$
                that satisfies
                \[
                    \dot{\vect{y}} = \vect{f}(t, \vect{y}), \quad \vect{y}(t_0) = \vect{y}_0    
                \]
                where $t_0 \in I$ is the \emph{initial time}, $\vect{y}_0 \in D$ 
                is the \emph{initial state} and together they are the \emph{initial conditions}.

                $I \subset \R$ is the \emph{time interval}, $t \in I$ the \emph{time variable}.

                $D \subset \R^N$ is the \emph{state space} or \emph{phase space} and $\vect{y} \in D$
                is the \emph{state variable}.

                We also define $\Omega := I \times D$ as the \emph{extended state space} of tuples $(t, \vect{y})$.
            \end{definition}

            \begin{thm}[Theorem of Peano \& Picard-Lindelöf]
                If the right hand side funciton is locally Lipschitz continuous 
                then for all initial conditions $(t_0, \vect{y}_0) \in \Omega$ the 
                IVP
                
                \[
                    \dot{\vect{y}} = \vect{f}(t, \vect{y}), \quad \vect{y}(t_0) = \vect{y}
                \]

                has a solution with maximal (temporal) domain of defintion.
            \end{thm}

            \subsubsubsection{Autonomization}
                Autonomization is the process of converting ODEs into autonomous 
                ODEs (time-independent).\\

                \begin{cor}
                    Autonomous ODEs already represent the general case, because 
                    every ODE can be converted into an autonomous one.
                \end{cor}

                The idea is to include time as an extra component of the state vector.
                We call the new state vector the \emph{extended state vector} $\vect{z} := \vect{z}(t)$.\\

                \[
                    \vect{z}(t) := \begin{bmatrix}
                        \vect{y}(t)\\
                        t
                    \end{bmatrix} 
                    \iff 
                    \begin{bmatrix}
                        z_1\\
                        \vdots\\
                        z_N\\
                        z_{N+1}
                    \end{bmatrix} 
                    := 
                    \begin{bmatrix}
                        y_1\\
                        \vdots\\
                        y_N\\
                        t
                    \end{bmatrix}
                \]

                \[
                    \dot{\vect{y}} = \vect{f}(t, \vect{y}) \iff
                    \dot{\vect{z}} = g(\vect{z})
                    \iff
                    \begin{bmatrix}
                        \dot{\vect{y}}\\
                        \odv{}{t}(t)
                    \end{bmatrix}
                    =
                    \begin{bmatrix}
                        \vect{f}(y_1, \cdots, y_N, t)\\
                        1
                    \end{bmatrix}
                \]

            \subsubsubsection{From higher order ODEs to first order systems}
                An ordinary differnetial equation of \emph{order} $n \in \mathbb{N}$
                has the form
                \[
                    \vect{y}^{(n)} = \vect{f}(t, \vect{y}, \dot{\vect{y}}, ..., \vect{y}^{(n-1)})
                \]

                All higher order ODEs ca be turned into a 1st-order ODE by adding
                derivatives up to order $n - 1$ as additional components to the 
                state vector. We define this \emph{extended state vector} 
                $\vect{z}(t) \in \R^{n \cdot d}$ as:
                \[
                    \vect{z}(t) := \begin{bmatrix}
                        \vect{y}(t)\\
                        \vect{y}^{(1)}(t)\\
                        \vdots\\
                        \vect{y}^{(n-1)}(t)
                    \end{bmatrix}
                    =
                    \begin{bmatrix}
                        \vect{z}_1\\
                        \vect{z}_2\\
                        \vdots\\
                        \vect{z}_n
                    \end{bmatrix}
                    \in \R^{N \cdot n}
                    \quad \iff \quad
                    \dot{\vect{z}} = \vect{g}(\vect{z}) := 
                    \begin{bmatrix}
                        \vect{z}_2\\
                        \vect{z}_3\\
                        \vdots\\
                        \vect{z}_n\\
                        \vect{f}(t,\vect{z}_1, ..., \vect{z_n})
                    \end{bmatrix}
                \]
                

        \subsubsection{Evolution Operators}

            For the sake of simplicity we restrict the discussion in this chapter 
            to autonomous initial value-problems:\\

            \[
                \dot{\vect{y}} = \vect{f}(\vect{y}), \quad \vect{y}(0) = \vect{y}_0
            \]

            We want to fix a duration $t \in \R \setminus \{ 0\}$ and follow
            all trajectories of an ODE this duration. This induces the mapping:
            \[
                \vect{\Phi}^t: \begin{cases}
                    D \mapsto D\\
                    \vect{y}_0 \mapsto \vect{y}(t)
                \end{cases}, \quad t \mapsto \vect{y}(t)
            \]
            This is a well-definied mapping of the state space into itself, for 
            a given $t$.

            We may also let $t$ vary which spawns a family of mappings 
            $\{ \vect{\Phi}^t \}_{t \in \R}$ of the state space $D$ into itself.
            However, it can also be expressed as a mapping with two arguments.

            \begin{definition}[Evolution operator/mapping]
                \cite[Def. 6.1.4.3]{hipt_npde}

                We assume an IVP and that its solutions are global.

                The mapping:
                \[
                    \vect{\Phi}: \begin{cases}
                        \R \times D \mapsto D,\\
                        (t, \vect{y}_0) \mapsto \vect{\Phi}^t \vect{y}_0 := \vect{y}(t)
                    \end{cases}    
                \]
                is a \emph{evolution operator/mapping} for the autonomous ODE 
                $\dot{\vect{y}} = \vect{f}(\vect{y})$ and $\vect{y}(t) \in C^1(\R, \R^N)$
                is the unique (global) solution
            \end{definition}

            By the definition we therfore have:
            \[
                \odv{\vect{\Phi}}{t}(t, \vect{y}) = \vect{f}(\vect{\Phi^t \vect{y}})
            \]

            The evolution operator includes information for two different operations:

            \begin{itemize}
                \item $t \mapsto \vect{\Phi}^t \vect{y}_0$ with $\vect{y}_0 \in D$ fixed: 
                    is a trajectory of an IVP.
                \item $\vect{y} \mapsto \vect{\Phi}^t\vect{y}$ with $t \in \R$ fixed:
                    is a mapping of the state space onto itself after the fixed duration.
            \end{itemize}

            There is a one-to-one relationship between ODEs and their evolution 
            operators and those are the key objects behind an ODE.

            \begin{quote}
                An ODE ``encodes'' an evolution operator.\cite{hipt_npde}
            \end{quote}

            Numerical integration is concerned with the approximation of evolution
            operators.

        \pagebreak
    \subsection{Introduction: Polygonal Approximation Methods}

            We are looking at an IVP for an first-order ODE, with the right-hand side given in procedural form.
            (Meaning only point-evaluations are possible).

            \[
                \dot{\vect{y}} = \vect{f}(t, \vect{y}), \quad \vect{y}(t_0) = \vect{y}_0
            \]

            Our goal is to approximate the problem on a temporal mesh.
            \[
                \mathcal{M} := \{ t_0 < t_1 < \cdots < t_{M-1} < t_M := T \} \subset [t_0, T]
            \]
            for autonomous IVPs: $t_0 = 0$

        \subsubsection{Explicit Euler Method}

            Idea: Follow the tangents over short periods of time. 
            We follow the tangets at a time point up to the next time point. For
            shorter time steps this approximates the solution.

            We also do this successively, meaning we continue from the approximated 
            solution at the next time step up until $T$.

            This is the \emph{Explicit Euler Method}.

            \begin{definition}
                Applied to a general IVP with a first-order ODE the \emph{explicit Euler method}
                generates a sequence $(\vect{y}_k)^N_{k=0}$ by the recursion
                \[
                    \vect{y}_{k+1} = y_k + h_k \, \vect{f}(t_k, \vect{y}_k), \quad k = 0, ..., M - 1
                \]
                with the local stepsize (timestep) $h_k := t_{k+1} - t_k$
            \end{definition}

            \begin{definition}[Difference scheme]
                A \emph{Differece Scheme} is a scheme that follows a simple policy 
                for the discretization of differential equations. We replace all
                derivatives by difference quotients connecting solution values on 
                a set of discrete points. Meaning difference quotients starting at 
                one point in the mesh to the next point in the mesh.
            \end{definition}

            \begin{lemma}[Explicit Euler Method as a Difference Scheme]
                By approximating the derivative by a \emph{forward
                difference quotient} on the temporal mesh $\mathcal{M} := \{
                t_0, t_1, ..., t_M \}$ we obtain:
                \[
                    \dot{\vect{y}}(t_k) \approx \frac{\vect{y}(t_k + h_k) - \vect{y}(t_k)}
                    {h_k} \qquad \text{for small} \  h_k
                \]

                And since $\dot{\vect{y}} = \vect{f}(t, \vect{y})$, we can write
                \[
                    \frac{\vect{y}_{k+1} - \vect{y}_k}{h_k} = \vect{f}(t_k, \vect{y}_h(t_k)),
                    \quad k = 0, ..., M - 1
                \]
                where the former is the \emph{forward difference quotient}.
            \end{lemma}

        \subsubsection{Implicit Euler Method}

            Instead of forward difference quotient we can also use the backward
            difference quotient.

            \begin{lemma}[Implicit Euler Method as a Difference Scheme]
                On a (temporal) mesh $\mathcal{M} := \{ t_0, t_1, ..., t_M \}$
                we obtain (using $\dot{\vect{y}} = f(t, \vect{y})$).
                \[
                    \frac{\vect{y}_{k+1} - \vect{y}_k}{h_k} = \vect{f}(t_{k+1}, \vect{y}_h(t_{k+1})),
                    \quad k = 0, ..., M - 1
                \]
                where the former is the \emph{backwards difference quotient}.
            \end{lemma}

            \begin{definition}[Implicit Euler Method]
                \[
                    \vect{y}_{k+1} = \vect{y}_k + h_k \, \vect{f}(t_{k+1}, \vect{y}_{k+1})
                    \quad k = 0, ..., M - 1
                \]
                with local stepsize (timestep) $h_k := t_{k+1} - t_k$.\\

                However, the Implicit Euler Method requires solving a (possibly
                non-linear) system of equations to obtain $\vect{y}_{k+1}$.
            \end{definition}

            \begin{lemma}[Feasibility of Implicit Euler timestepping]
                \cite[Remark 6.2.2.3]{hipt_npde}

                We want to know wether we can solve the Implicit Euler method
                for $\vect{y}_{k+1}$, that is wether it is well-definied and its
                solution is unique.

                $\Rightarrow$ for sufficiently small $h > 0$ the Implicit Euler
                method has a unique solution $\vect{y}_{k+1}$.
            \end{lemma}

        \subsubsection{Implicit Midpoint Method}

            We can also use the \emph{symmetric difference quotient} instead of
            the forward or backward difference methods to arrive at the 
            \emph{Implicit Midpoint method}.

            The idea is to apply the following formula in $t = \onehalf(t_k + t_{k+1})$
            \[
                \dot{\vect{y}}(t) \approx \frac{\vect{y}(t + h) - \vect{y}(t - h)}{2h}, \quad h>0 
            \]
            with $h = \frac{h_k}{2}$ which transforms the ODE $\dot{\vect{y}} = \vect{f}(t, \vect{y})$ into
            \[
                \frac{\vect{y}_{k+1} - \vect{y}_k}{h_k} 
                = \vect{f}\left(\onehalf(t_k + t_{k+1}), \, \vect{y}_h\left(\onehalf(t_k + t_{k+1})\right)\right)
            \]

            The problem is the value $\vect{y}_h\left(\onehalf(t_k + t_{k+1})\right)$ does not seem to be available.
            However, we remember that the approximate trajectory is supposed to be piecewise linear, which implies
            \[
                \vect{y}_h \left(\onehalf(t_k + t_{k+1}) \right) = \onehalf(\vect{y}_h(t_k) + \vect{y}_h(t_{k+1}))
            \]
            giving us the \emph{Implicit Midpoint method}.

            \begin{definition}[Implicit Midpoint Method]
                
                \[
                    \vect{y}_{k+1} = \vect{y}_k 
                    + h_k \, \vect{f}\left(\onehalf(t_k + t_{k+1}), \onehalf(\vect{y}_k + \vect{y}_{k+1})\right)
                \]
            \end{definition}

        \pagebreak
    \subsection{General Single-Step Methods}

        \subsubsection{Definition}

            We recall from the previous sections the two Euler methods for an 
            autonomous ODE $\dot{\vect{y}} = \vect{f}(\vect{y})$:
            \begin{itemize}
                \item Explicit Euler: $\vect{y}_{k+1} = \vect{y}_k + h_k \, \vect{f}(\vect{y}_k)$
                \item Implicit Euler: $\vect{y}_{k+1} = \vect{y}_k + h_k \, \vect{f}(\vect{y}_{k+1})$
            \end{itemize}
            
            Both formulas provide a mapping, for sufficiently small $h_k$
            \[
                (\vect{y}_k, h_k) \mapsto \vect{\Psi}(h_k, \vect{y}_k) := \vect{y}_{k+1}
            \]\\

            Let's say we have the initial value $\vect{y}_0$. We can get the next step
            using the mapping we defined above $\vect{y}_1 := \vect{\Psi}(h, \vect{y}_0)$.\\

            This can be regarded as an approximation of $\vect{y}(h)$, which is
            the value returned by the evolution operator $\vect{\Phi}$ of an ODE
            when applied to $\vect{y}_0$ over the period $h$.\\
            \[
                \vect{y}_1 \approx \vect{y}(h) = \vect{\Phi}^h \vect{y}_0
            \]
            
            Or as Ralf put it in the lecture document:
            \begin{quote}
                ``This is what every Single-Step method does: It approximates the 
                evolution operator $\vect{\Phi}$ for an ODE by a mapping $\vect{\Psi}$.''
                \cite[6.3.1.1]{hipt_npde}
            \end{quote}

            \begin{definition}[Discrete evolution]
                The mapping $\vect{\Psi}$ is a \emph{discrete evolution operator}
                \[
                    \vect{\Psi}(h, \vect{y}) \approx \vect{\Phi}^h \vect{y}
                \]

                In analogy to $\vect{\Phi}^h$ for discrete evolutions we often 
                write $\vect{\Psi}^h \vect{y} := \vect{\Psi}(h, \vect{y})$
            \end{definition}

            \begin{definition}[Single-step method]
                Def. 6.4.1.4

                Given a \emph{discrete evolution} $\vect{\Psi}: \ \Omega \subset 
                \R \times D \mapsto \R^N$, an initial state $\vect{y}_0$, and a 
                temporal mesh $\mathcal{M} := \{ 0 =: t_0 < t_1 < \cdots < t_M := T \},
                M \in \mathbb{N}$, the recursion
                \[
                    \vect{y}_{k+1} := \vect{\Psi}(t_{k+1} - t_k, \, \vect{y}_k), \quad
                    k = 0, ..., M - 1
                \]
                defines a single-step method (SSM) for the autonomous IVP 
                $\dot{\vect{y}} = \vect{f}(\vect{y}), \ \vect{y}(0) = \vect{y}_0$
            \end{definition}

            \begin{definition}[Consistent Discrete Evolution]
                6.3.1.10

                A consistent discrete evolution $\vect{\Psi}$ definining a 
                Single-Step method for an autonomous ODE is of the form:
                \[
                    \vect{\Psi}^h \vect{y} = \vect{y} + h \vect{\psi}(h, \vect{y})
                    \quad \text{with} \quad \begin{array}{l}
                        \vect{\psi}: I \times D \to \R^N \ \text{continuous}\\
                        \vect{\psi}(0, \vect{y}) = \vect{f}(\vect{y})
                    \end{array}
                \]
            \end{definition}

            \begin{definition}[Consistent Single-Step Method]
                Def. 6.3.1.11

                A Single-Step method based on a consistent discrete evolution 
                is called \emph{consistent} with the ODE.
            \end{definition}

        \subsubsection{(Asymptotic) Convergence of Single-Step Methods}

            We are concerened with the accuary of the solution sequence obtained
            by a single-step mehod. We will now study suitable norms of the so called
            discretization error on the choice of a temporal mesh.\\

            We focus on $h$-convergence and consider the family of temporal
            meshes with $h_\mathcal{M} \to 0$.
            \[
                h_\mathcal{M} := \max_k h_k
            \]

            \begin{definition}[Discretization Error]
                Approximation errors in numerical integration are also called
                \emph{discretization errors}.

                We use different norms of discretization errors for different 
                objectives.

                \begin{itemize}
                    \item Only seeking solution at a final time $T$:\\
                        We use:
                        \[
                            \epsilon_M := \norm{\vect{y}(T) - \vect{y}_M}
                        \]
                    \item Approximating the trajectory:\\
                        We use the function: $t \mapsto \vect{e}(t)$
                        \[
                            \vect{e}(t) := \vect{y}(t) - \vect{y}_h(t)
                        \]
                    \item Pointwise discretization error:\\
                        \[
                            \vect{e}: \mathcal{M} \to D, \quad \vect{e}_k := 
                            \vect{y}(t_k) - \vect{y}_k, \quad k = 0, ..., M
                        \]
                        But we usually just examine the \emph{maximum error} in the
                        mesh points
                        \[
                            \norm{(\vect{e})}_\infty := \max_{k \in \{1, ..., M \}}
                            \norm{\vect{e}_k} = \max_{k \in \{1, ..., M \}} \vect{y}(t_k) - \vect{y}_k
                        \]
                \end{itemize}
            \end{definition}

            \begin{thm}[Algebraic convergence of Single-Step methods]
                Assuming an IVP with sufficiently smooth right hand side function $\vect{f}$.\\

                Conventional single-step methods will enjoy asymptotic algebraic
                convegence in the meshwidth, more precisely:\\

                There is a $p \in \mathbb{N}$ such that the sequence generated by a SSM
                on a mesh satisfies:
                \[
                    \max_k \norm{\vect{y}_k - \vect{y}(t_k)} \leq C h^p \quad \text{for} \quad
                    h := \max_{k} |t_k - t_{k-1}| \to 0
                \]
                with $C > 0$, independent of $\mathcal{M}$.
            \end{thm}

            \begin{definition}[Order of a Single-Step Method]
                The maximal integer $p \in \mathbb{N}$ for which the algebraic convegence holds
                for a SSM when applied to an ODE with sufficiently smooth right hand side,
                is caled the \emph{order} of the method.
            \end{definition}

            Let's look at the simplest single step method, the explicit Euler method.
            (With sufficiently smooth and globally Lipschitz continuous $\vect{f}$.)

            We want to estimate the error sequence $\norm{\vect{e}_k}$ (green arrows).

            // TODO insert grapic. (6.3.2.9)

            The approach to estimate the error sequence compromises of three key steps.\\

            \subsubsubsection{Step 1: Splitting}

                The first step is the abstract splitting of the error.

                \begin{lemma}[Error splitting]
                    \begin{align*}
                        \vect{e}_{k+1} &= \vect{\Psi}^{h_k}\vect{y}_k - \vect{\Phi}^{h_k}\vect{y}(t_k)\\
                        &= \underbrace{\vect{\Psi}^{h_k}\vect{y}_k - \vect{\Psi}^{h_k}\vect{y}(t_k)}_\text{propagated error} 
                        + \underbrace{\vect{\Psi}^{h_k}\vect{y}(t_k) - \vect{\Phi}^{h_k}\vect{y}(t_k)}_\text{one-step error}
                    \end{align*}
                \end{lemma}

                \subsubsubsection{Step 2: One-step error}

                The second step is to estimate the one-step error ($\vect{\tau}$).

                We recall

                \begin{thm}[Estimate for one-step error]
                    We observe 
                    \[
                        \vect{\tau}(h_k, \vect{y}(t_k)) = O(h_k^2)
                    \]
                    uniformly for $h_k \to 0$.
                \end{thm}

            \subsubsubsection{Step 3: Propagated error}

                The third step is to estimate the propagated error.

                \begin{thm}[Estimate for propagated error]
                    \[
                        \epsilon_{k+1} \leq (1 + h_k L)\epsilon_k + \rho_k, \quad
                        \rho_k := \onehalf h^2_k \max_{t_k \leq \tau \leq t_{k+1}} \norm{\ddot{\vect{y}}(\tau)}
                    \]
                \end{thm}

                The total error arises from accumulation of propagated one-step errors.

            \subsubsubsection{One-step error and order of single step method}

                The following theorem is true for almost all single step methods.

                \begin{thm}[Order of Algebraic Convergence fo Single-Step Methods]
                    6.3.2.22

                    Consider an IVP and a SSM defined by $\vect{\Psi}$. If the one-step
                    error along the solution trajectory satisfies 
                    \[
                        \norm{\vect{\Psi}^h \vect{y}(t) - \vect{\Phi}^h \vect{y}(t)}
                        \leq C h^{p+1} \quad \forall h \ \text{sufficiently small}, \ t \in [0, T]
                    \]
                    for some $p \in \mathbb{N}$ and $C > 0$, then, usually
                    \[
                        \max_k \norm{\vect{y}_k - \vect{y}(t_k)} \leq \bar{C} h^p_\mathcal{M}
                    \]
                    with $\bar{C} > 0$ independent of the temporal mesh $\mathcal{M}$: 
                    The pointwise discretization error \emph{converges algebraically} 
                    with \emph{order}/rate $p$.
                \end{thm}

            \subsubsubsection{Order of finite-difference SSMs}

                // TODO 6.3.2.25

        \pagebreak
    \subsection{Explicit Runge-Kutta Single-Step Methods}

        In this chapter we look at higher order single-step methods.

        \subsubsubsection{Rationale for high-order SSMs}

            We need high-order SSMs. They are highly advisable for the sake of 
            efficiency.
            The order of an SSM corrolates to the rate of asymptotic algebraic convergence.\\

            How much additional effort do we have to invest for a desired gain in accuracy?\\

            The computational effort is the total number of $\vect{f}$-evaluations to 
            approximately solving the IVP. The number of tmesteps dictates the number of 
            $\vect{f}$-evaluations, which is proportional to $h^{-1}$.

            We can measure the gain in accuracy by the change from the old error to the new one.
            \[
                \frac{\text{err}(h_\text{old})}{\text{err}(h_\text{new})} = \rho
            \]
            $\rho > 1$ is called the reduction factor.

            \[
                h_\text{new} = \rho^{-\frac{1}{p}} \cdot h_\text{old}
            \]

            \begin{thm}
                For a single-step method of order $p \in \mathbb{N}$
                \[
                    \text{increase effort by factor} \ \rho^{\frac{1}{p}} \quad
                    \Rightarrow \quad \text{reduce error by factor} \ \rho > 1
                \]
            \end{thm}

        \subsubsubsection{Bootstrap construction of explicit single step methods}

            Now we will build a class of explicit methods that achieve orders of $p > 2$.
            We start at a simple integral equation.

            \[
                \begin{array}{l}
                    \dot{\vect{y}}(t) = \vect{f}(t, \vect{y}(t))\\
                    \vect{y}(t_0) = \vect{y}_0
                \end{array} \quad \Rightarrow \quad
                \vect{y}(t_1) = \vect{y}_0 + \int^{t_1}_{t_0} \vect{f}(\tau, \vect{y}(\tau)) \ d\tau
            \]

            Idea: Approximate the integral by using $s$-point quadrature on $[0,1]$
            with nodes $c_1, ..., c_s$ and weights $b_1, ..., b_s$

            \[
                \vect{y}(t_1) \approx \vect{y}_1 = \vect{y}_0 h \sum_{i=1}^s b_i \,
                \vect{f}\bigl(t_0 + c_i h, \, \vect{y}(t_0 + c_i h)\bigr), \quad h := t_1 - t_0
            \]

            We want to obtain the values $\vect{y}(t_0 + c_i h)$ by bootstrapping.\\

            In this case \emph{bootstrapping} means using a simpler/cheaper (lower-order)
            SSM to get these values. (More on that later)\\

            What error can we afford in the approximation of $\vect{y}(t_0 + c_i h)$?
            We aim for a one-step error bound of:

            \[
                \vect{y}(t_1) - \vect{y}_1 = O(h^{p+1}) 
            \]

            Note the factor in front of the quadrature sum. Thus, our goal can 
            alrady be achieved if $\vect{y}(t_0 + c_i h)$ is approximated up to 
            an error $O(h^p)$.\\

            This is accomplished by a less accurate discrete evolution than the 
            one we are about to build. Thus we can construct discrete evolutions of
            higher and higher order. All these methods will be explicit, that is,
            $\vect{y}_1$ can only be compted directly from point values of $\vect{f}$.

        \begin{example}[Simple Runge-Kutta methods by quadrature \& bootstrapping]

            We now apply the ideas from above. We write $\vect{k}_\ell \in \R^N$
            for the approximations of $\vect{y}(t_0 + c_i h)$.

            // TODO
        \end{example}

        \begin{example}[Convergence of simple RK-SSMs]
            // TODO
        \end{example}

        \begin{definition}[Explicit Runge-Kutta method]
            // TODO
        \end{definition}

        \begin{definition}[Butcher scheme notation for explicit RK-SSM]
            // TODO
        \end{definition}

        \begin{recipe}[Butcher schemes for some explicit RK-SSMs]
                // TODO
        \end{recipe}

        \pagebreak
    \subsection{Adaptive Stepsize Control}

        \subsubsection{The Need for Timestep Adaptation}
            We need to use highly non-uniform temporal meshes for highly non-
            uniform solutions in time.

            Example: Finite-time blow up: $t = \frac{1}{y_0}$

        \subsubsection{Local-in-time Stepsize Control}

            We need an Algorithm for building a temporal mesh $\mathcal{M}$ during
            execution of SSM.\\

            We try to `curb'/`balance' the one-step error by adjusting the current 
            stepsize $h_k$. Therefore we need local-in-time one step error estimation.
            However, we don't have the local-in-time one-step error because we don't
            know the solution.\\

            We need to estimate the one-step error local-in-time.

            \subsubsubsection{Local-in-time error estimation}

                The idea is to compare results for two discrete evolutions of 
                different order over current timestep $h$.

                For $\text{Order}(\tilde{\vect{\Psi}}) > \text{Order}(\vect{\Psi})$, 
                we expect:
                \[
                    \vect{\Phi}^h \vect{y}_k - \vect{\Psi}\vect{y}_k 
                    \approx \tilde{\vect{\Psi}}^h\vect{y}_k - \vect{\Psi}^h\vect{y}_k 
                    =: \text{EST}_k
                \]

            \subsubsubsection{Local timestep adaptation}

                We now simply compare the local-in-time error estimation with a 
                given tolerance to accept or reject the step and the stepsize.

                \[
                    \text{EST}_k \leq \text{TOL} 
                    := \max\{ \text{ATOL}, \ \text{RTOL} \cdot \norm{\vect{y}_k} \}
                \]
                \begin{itemize}
                    \item If True: Accept the step and advance to the next step.
                    \item If Falspe: reject step and stepsize, repeat with $h \leftarrow \frac{1}{2} h$.
                \end{itemize}
                Also halt if blow-up is detected.

            \subsubsubsection{Simple adaptive stepsize control and its failure}

                

            \subsubsubsection{Refined local stepsize control}

        \subsubsection{Embedded Runge-Kutta Methods}

            \subsubsubsection{Extended Butcher scheme}