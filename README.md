# NumPDE Summary 2022 -  Lukas Bühler

> Disclaimer: This summary is not complete! It is no replacement but should only
> serve as an addition to the course material.

It contains some of my takeaways and the most important definitions, in my opinion.

The summary is heavily based on the course lecture notes and lecture document.

It is based on the spring 2022 lecture of Numerical Methods for Partial Differential
equations by Ralf Hiptmair.


